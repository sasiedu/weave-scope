#!/bin/bash

# ============================================================
# Set version tag number
# ============================================================
#TAG=$(git rev-parse --short HEAD)
TAG=1.7.3

# ============================================================
# Build image
# ============================================================
echo docker build -f Dockerfile --tag foundery.azurecr.io/foundery/kafka/cli:${TAG} .
docker build -f Dockerfile --tag foundery.azurecr.io/foundery/kafka/cli:${TAG} .

# ============================================================
# Add tags
# Note: $BUILD_NUMBER is a TeamCity environment variable
# ============================================================
#docker tag nexus.foundery.club/foundery/kafka/cli:${TAG} nexus.foundery.club/foundery/kafka/cli:${PRE}$(if [[ ${BUILD_NUMBER} ]]; then echo ${BUILD_NUMBER}; else echo "local"; fi)
#docker tag nexus.foundery.club/foundery/kafka/cli:${TAG} nexus.foundery.club/foundery/kafka/cli:${PRE}latest
#
#docker tag nexus.foundery.club/foundery/kafka/cli:${TAG} foundery.azurecr.io/foundery/kafka/cli:${TAG}
#docker tag nexus.foundery.club/foundery/kafka/cli:${TAG} foundery.azurecr.io/foundery/kafka/cli:${PRE}$(if [[ ${BUILD_NUMBER} ]]; then echo ${BUILD_NUMBER}; else echo "local"; fi)
docker tag foundery.azurecr.io/foundery/kafka/cli:${TAG} foundery.azurecr.io/foundery/kafka/cli:latest