#!/bin/bash

# ============================================================
# Set version tag number
# ============================================================
#TAG=$(git rev-parse --short HEAD)
TAG=1.7.3

# ============================================================
# Build image
# ============================================================
echo docker build -f Dockerfile --tag foundery.azurecr.io/foundery/weavescope/agent:${TAG} .
docker build -f Dockerfile --tag foundery.azurecr.io/foundery/weavescope/agent:${TAG} .

# ============================================================
# Add tags
# Note: $BUILD_NUMBER is a TeamCity environment variable
# ============================================================
#docker tag nexus.foundery.club/foundery/weavescope/agent:${TAG} nexus.foundery.club/foundery/weavescope/agent:${PRE}$(if [[ ${BUILD_NUMBER} ]]; then echo ${BUILD_NUMBER}; else echo "local"; fi)
#docker tag nexus.foundery.club/foundery/weavescope/agent:${TAG} nexus.foundery.club/foundery/weavescope/agent:${PRE}latest
#
#docker tag nexus.foundery.club/foundery/weavescope/agent:${TAG} foundery.azurecr.io/foundery/weavescope/agent:${TAG}
#docker tag nexus.foundery.club/foundery/weavescope/agent:${TAG} foundery.azurecr.io/foundery/weavescope/agent:${PRE}$(if [[ ${BUILD_NUMBER} ]]; then echo ${BUILD_NUMBER}; else echo "local"; fi)
docker tag foundery.azurecr.io/foundery/weavescope/agent:${TAG} foundery.azurecr.io/foundery/weavescope/agent:latest